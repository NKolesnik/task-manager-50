package ru.t1consulting.nkolesnik.tm.dto.response.user;

import lombok.NoArgsConstructor;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public class UserLogoutResponse extends AbstractResultResponse {

}
