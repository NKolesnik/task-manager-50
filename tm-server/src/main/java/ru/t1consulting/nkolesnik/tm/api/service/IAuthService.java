package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.SessionDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
